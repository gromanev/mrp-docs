Система Согласования Договоров: Документация
============================================

В данной документации будет изложена инструкция по установке и обслуживанию системы согласования договоров.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Введение

   pages/introduction/gettingstarted
   pages/introduction/struct
   pages/introduction/contents
   
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Установка и настройка

   pages/setup/prepare
   pages/setup/install
   pages/setup/sources
   pages/setup/settings

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Прочее

   pages/other/promt



