# Шпаргалка по командам

**Скачивание образов** - ``docker pull``
```
docker pull repo.treescale.com/gromanev/lynx-server:latest
docker pull repo.treescale.com/gromanev/lynx-client:latest
docker pull repo.treescale.com/gromanev/lynx-file-server:latest
docker pull repo.treescale.com/gromanev/lynx-identity:latest
docker pull repo.treescale.com/gromanev/lynx-admin:latest
```

**Загрузка образов** - ``docker load``
```
docker load --input /lynx/images/lynx-server.tar
docker load --input /lynx/images/lynx-client.tar
docker load --input /lynx/images/lynx-file-server.tar
docker load --input /lynx/images/lynx-identity.tar
docker load --input /lynx/images/lynx-admin.tar
```

**Сохранение образов** - ``docker save``
```
docker save --output c:\images\lynx-server.tar lynx-server
docker save --output c:\images\lynx-client.tar lynx-client
docker save --output c:\images\lynx-file-server.tar lynx-file-server
docker save --output c:\images\lynx-identity.tar lynx-identity
docker save --output c:\images\lynx-admin.tar lynx-admin
```

**Работа с docker-compose** - ``docker-compose``
```
docker-compose --file docker-compose.lynx.yml down
docker-compose --file docker-compose.lynx.yml up --build -d
```

**Создание Backup через pgAdmin 4**

![Backup 1](../../images/backup-1.png)


![Backup 2](../../images/backup-2.png)

**Восстановление БД через pgAdmin 4**

![restore 1](../../images/restore-1.png)


![restore 2](../../images/restore-2.png)


![restore 3](../../images/restore-3.png)
